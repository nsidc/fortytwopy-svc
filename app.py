#!/usr/bin/env python

from flask import Flask
from fortytwopy.fortytwo import answer


app = Flask(__name__)


@app.route('/')
def index():
    return str(answer())


@app.route('/info')
def info():
    return ('The Answer to the Ultimate Question of Life, the Universe, and '
            'Everything as a Service')


@app.route('/help')
def help():
    return "DON'T PANIC"


if __name__ == '__main__':
    app.run()

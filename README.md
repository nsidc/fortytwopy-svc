FortyTwo Python Service
---

A Python Flask web service to demonstrate how to test, build, and
publish a Docker image to [Docker Hub](https://hub.docker.com/r/nsidc/fortytwopy-svc/)
using [CircleCI](https://circleci.com/bb/nsidc/fortytwopy-svc).

* [![CircleCI](https://circleci.com/bb/nsidc/fortytwopy-svc.svg?style=svg)](https://circleci.com/bb/nsidc/fortytwopy-svc)

* [DockerHub: fortytwopy-svc](https://hub.docker.com/r/nsidc/fortytwopy-svc/)

Prerequisites
---

* [Miniconda3](https://conda.io/miniconda.html)

Development
---

Install dependencies:

        $ conda env create -f environment.yml
        $ source activate fortytwopy-svc

Run locally:

        $ FLASK_APP=app.py flask run

Build and run it via Docker:

        $ docker build -t fortytwopy-svc:dev .
        $ docker run -p 5000:5000 --name 42 --rm -it fortytwopy-svc:dev

Workflow
---

TL;DR: Use
[GitHub Flow](https://guides.github.com/introduction/flow/index.html).

In more detail:

1. Create a feature branch.
2. Create and push commits on that branch.
3. The feature branch will get built on CircleCI with each push.
4. Update the CHANGELOG with description of changes.
5. Create a Pull Request on BitBucket.
6. When the feature PR is merged, master will get built on CircleCI
   and a Docker image with a tag of 'latest' pushed to DockerHub.

Releasing
---

1. Update the CHANGELOG to list the new version.
2. Add files and commit

        $ git add CHANGELOG.md ...
        $ git commit -m "Release v.X.Y.Z"

3. Create a new version tag:

        $ git tag v1.2.3

4. Push:

        $ git push origin master --tags

The new version will get built in CircleCI and pushed
to [Docker Hub](https://hub.docker.com/) with the corresponding
version as a Docker image tag.

Installing and Running
---

To install and run the latest version:

        $ docker pull nsidc/fortytwopy-svc:latest
        $ docker run -p 5000:5000 --name fortytwopy-svc -it nsidc/fortytwopy-svc:latest

To install and run a specific version:

        $ docker pull nsidc/fortytwopy-svc:v1.2.3
        $ docker run -p 5000:5000 --name fortytwopy-svc -it nsidc/fortytwopy-svc:v1.2.3

License
---

Copyright 2018 National Snow & Ice Data Center
<programmers@nsidc.org>.

This code is licensed and distributed under the terms of the MIT
License (see LICENSE).
